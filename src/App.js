import React, { Component } from "react"
import "bootstrap/dist/css/bootstrap.min.css"
import NavMenu from "./components/NavMenu"
import MyForm from "./components/MyForm"
import { Col, Container, Row } from "react-bootstrap"
import MyTable from "./components/MyTable"

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            data: [{
                username: "",
                gender: "",
                password: "",
                isSelected: true,
            }],
        }
        this.onGetData = this.onGetData.bind(this)
        this.onDelete = this.onDelete.bind(this)
    }

    onDelete() {
        let afterDelete = this.state.data.filter(item => {
            return item.isSelected !== true
        })
        this.setState({
            data: afterDelete
        })
    }

    onGetData(data) {
        console.log("data:", data);
        let addData = {
            username: data.username,
            gender: data.gender,
            email: data.email,
            password: data.password,
            isSelected: false
        }
        let newData = [...this.state.data, addData]
        this.setState({
            data: newData
        })
    }


    render() {
        return (
            <div>
                <NavMenu />
                <Container>
                    <Row>
                        <MyForm
                            onGetData={this.onGetData} />
                        <Col>
                            <MyTable
                                items={this.state.data}
                                onDelete={this.onDelete} />
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

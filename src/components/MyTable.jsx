import React from 'react'
import { Table, Button } from 'react-bootstrap'

export default function MyTable(props) {
    console.log("Props on Function:", props.items);
    const temp = props.items.filter(item => {
        return item.username !== ""
    })

    console.log("temp data", temp);

    return (
        <div>
            <h3>Table Accounts</h3>
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Gender</th>
                    </tr>
                </thead>
                <tbody>
                    {temp.map((item, index) => (
                        <tr
                            key={index}
                            onClick={() => {
                                item.isSelected = !item.isSelected
                            }}>
                            <td>{index + 1}</td>
                            <td>{item.username}</td>
                            <td>{item.email}</td>
                            <td>{item.gender}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>

            <Button
                variant="danger" 
                type="button" 
                onClick={() => {
                    props.onDelete()
                }}>
                    Delete
            </Button>
        </div>
    )
}

import React, { Component } from 'react'
import { Form, Button, Image } from 'react-bootstrap'

class MyForm extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    onChangeData = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <Form>
                <label htmlFor="myfile">
                    <Image
                        width="200px"
                        height="200px"
                        src="image/coount.png"
                        rounded />
                </label>

                <h5>Create Account</h5>

                <Form.Group controlId="formBasicUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                        name="username"
                        onChange={this.onChangeData}
                        type="name"
                        placeholder="Username" />
                </Form.Group>

                <Form.Group controlId="formBasicGender">
                    <Form.Label>Gender</Form.Label>
                    <br></br>
                    <Form.Check
                        custom
                        inline
                        label="Male"
                        type="radio"
                        id="male"
                        value="Male"
                        name="gender"
                        onChange={this.onChangeData}/>
                    <Form.Check
                        custom
                        inline
                        label="Female"
                        type="radio"
                        id="female"
                        name="gender"
                        onChange={this.onChangeData}
                        defaultChecked={true} />
                </Form.Group>

                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        name="email"
                        onChange={this.onChangeData}
                        type="email"
                        placeholder="Email" />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        name="password"
                        onChange={this.onChangeData}
                        type="password"
                        placeholder="Password" />
                </Form.Group>

                <Button
                    variant="primary"
                    type="button" onClick={() => {
                        this.props.onGetData(this.state)
                    }}>
                    Save
                </Button>
            </Form>
        )
    }
}

export default MyForm;